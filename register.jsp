<%@page language="java" contentType="text/html; charset=utf-8"
        pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org./TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html: charset=utf-8">
        <title>Coffee-machine register</title>
    </head>
    <body>
        <c:if test="${empty _Username}">
            <c:set var="_Username" value="Username" scope="session"/>
            <c:set var="_Password" value="Password" scope="session"/>
            <c:set var="_Login" value="Login" scope="session"/>
            <c:set var="_Register" value="Register" scope="session"/>
            <c:set var="_Name" value="Name" scope="session"/>
            <c:set var="_Password_again" value="Password again" scope="session"/>
            <c:set var="_Account_N" value="Account_N" scope="session"/>
            <c:set var="_Home" value="Home" scope="session"/>
        </c:if>
        <table>
                <form action="Project6" method="post">
                    <input type="hidden" name="command" value="register"/>
                    <tr>
                        <td>
                            ${_Name}
                        </td>
                        <td>
                            <input type="text" name="name" value="${name}">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ${_Username}
                        </td>
                        <td>
                            <input type="text" name="user" value="${user}">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ${_Password}
                        </td>
                        <td>
                            <input type="password" name="password" value="${password}">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ${_Password_again}
                        </td>
                        <td>
                            <input type="password" name="password2" value="${password2}">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            ${_Account_N}
                        </td>
                        <td>
                            <input type="text" name="account_N" value="${account_N}">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type="submit" value="${_Register}"/>
                        </td>
                </form>
                        <td>
                            <form action="index.jsp" method="get">
                                <input type="submit" value="${_Home}"/>
                            </form>
                        </td>
                        <td>
                            <form action="Project6" method="post">
                                <input type="hidden" name="command" value="langchange"/>
                                <input type="hidden" name="lang" value="EN"/>
                                <input type="hidden" name="page" value="registerpage"/>
                                <input type="submit" value="EN"/>
                            </form>
                        </td>
                        <td>
                            <form action="Project6" method="post">
                                <input type="hidden" name="command" value="langchange"/>
                                <input type="hidden" name="lang" value="RU"/>
                                <input type="hidden" name="page" value="registerpage"/>
                                <input type="submit" value="RU"/>
                            </form>
                        </td>
                    </tr>
        </table>
        <br>        
        <h1>${message}</h1>
    </body>
</html>