package by.epam.felix.kam.project6.connectionspool;

import by.epam.felix.kam.project6.connectionspool.ConnectionsPool;

import java.sql.Connection;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Pool Testing.
 * 
 * @author Felix
 */
public class ConnectionsPoolTest {
	public static Logger logger = Logger.getLogger(ConnectionsPoolTest.class.getName());
	static {
	new DOMConfigurator().doConfigure("../webapps/project6/WEB-INF/log4j.xml", LogManager.getLoggerRepository());
	}
	class TestPool implements Runnable {
		ConnectionsPool pool;
		public TestPool(ConnectionsPool pool){
			this.pool=pool;
		}
		
        public void run() {
        	try{
	        	Connection con;
				con=pool.getConection();
			    pool.returnConection(con);
        	} catch (Exception e){
        		e.printStackTrace();
        	}
        }
    }
	
    public ConnectionsPoolTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test(timeout = 1000)
    public void testCaptureReleaseConnections() throws Exception {
    	logger.info("Testing pool.");
    	try{
			ConnectionsPool pool=new ConnectionsPool("jdbc:mysql://localhost:3306/coffee_mashine","coffee","12",4);
			Connection con1;
			Connection con2;
			Connection con3;
			Connection con4;
			Connection con5;
	    	
			con1=pool.getConection();
			con2=pool.getConection();
			con3=pool.getConection();
			con4=pool.getConection();
			pool.returnConection(con4);
			con5=pool.getConection();
			
		    if (!con5.equals(con4)) {
            	logger.error("Not the same connection.");
	            fail("Not the same connection.");
	        }
		    
		    
	        Thread testTread = new Thread(new TestPool(pool));
	        testTread.start();
		    try {
	            testTread.join(500l);

	            if (!testTread.isAlive()) {
	            	logger.error("TestThread die.");
	                fail("TestThread die.");
	            }


			    pool.returnConection(con5);
	            testTread.join(100l);
	            
	            if (testTread.isAlive()) {
	            	logger.error("TestThread is still alive.");
	                fail("TestThread is still alive.");
	            }
	        } catch (InterruptedException e) {
	        	logger.error("Error waining TestThread.",e);
	            fail("Error waining TestThread.");
	        }
		    
		    pool.returnConection(con1);
		    pool.returnConection(con2);
		    pool.returnConection(con3);
			
    	} catch(Exception e){
    		logger.error("Exception in test",e);
    		throw e;
    	}
    	logger.info("Testing pool finished.");
    }

}
