package by.epam.felix.kam.project6.command;

import by.epam.felix.kam.project6.myexception.ICommandException;

public interface ICommand {
	public void execute() throws ICommandException;
}
