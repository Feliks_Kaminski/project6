package by.epam.felix.kam.project6.command.commands;

import java.util.Set;

import by.epam.felix.kam.project6.Project6;
import by.epam.felix.kam.project6.command.ICommand;
import by.epam.felix.kam.project6.order.Ingredient;
import by.epam.felix.kam.project6.order.Order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
/**
 * Adds Ingredient into last Drink in Order
 * 
 * @author Felix
 *
 */
public class CommandAddIngredient implements ICommand {

	private HttpServletRequest request;
	private HttpServletResponse response;

	public static Logger logger = Logger.getLogger(CommandAddIngredient.class
			.getName());

	public CommandAddIngredient(HttpServletRequest request,
			HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	@Override
	public void execute() {
		HttpSession session = request.getSession(false);
		Order order = (Order) session.getAttribute("order");
		if (order.size() == 0) {
			request.setAttribute("message", session.getAttribute("_Ingredients_in_drinks"));
		} else {
			String ingredientName = request.getParameter("ingredient");
			int ingredientId = Integer.parseInt(request
					.getParameter("ingredientId"));
			logger.info(ingredientName);
			logger.info(ingredientId);
			for (Ingredient ingredient : (Set<Ingredient>) Project6.link()
					.getServletContext().getAttribute("ingredients")) {
				if (ingredient.getIngredientId() == ingredientId) {
					Ingredient i = new Ingredient(ingredientName,
							ingredient.getIngredientId());
					i.setPrice(ingredient.getPrice());
					order.getDrink(order.size() - 1).addIngredient(i);
					break;
				}
			}
		}
		new CommandViewPage(request, response).execute();

	}
}
