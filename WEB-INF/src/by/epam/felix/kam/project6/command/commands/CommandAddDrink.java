package by.epam.felix.kam.project6.command.commands;

import java.util.Set;

import by.epam.felix.kam.project6.Project6;
import by.epam.felix.kam.project6.command.ICommand;
import by.epam.felix.kam.project6.order.Drink;
import by.epam.felix.kam.project6.order.Order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
/**
 * Adds Drink to Order
 * 
 * @author Felix
 *
 */
public class CommandAddDrink implements ICommand {
	private HttpServletRequest request;
	private HttpServletResponse response;

	public static Logger logger = Logger.getLogger(CommandAddDrink.class
			.getName());

	public CommandAddDrink(HttpServletRequest request,
			HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	@Override
	public void execute() {
		HttpSession session = request.getSession(false);
		Order order = (Order) session.getAttribute("order");
		String drinkName = request.getParameter("drink");
		int drinkId = Integer.parseInt(request.getParameter("drinkId"));
		logger.info(drinkName);
		for (Drink drink : (Set<Drink>) Project6.link().getServletContext()
				.getAttribute("drinks")) {
			if (drink.getDrinkId() == drinkId) {
				Drink d = new Drink(drinkName, drink.getDrinkId());
				d.setPrice(drink.getPrice());
				order.addDrink(d);
				break;
			}
		}
		new CommandViewPage(request, response).execute();

	}

}
