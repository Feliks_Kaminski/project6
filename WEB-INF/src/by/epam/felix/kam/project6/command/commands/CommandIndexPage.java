package by.epam.felix.kam.project6.command.commands;

import java.io.IOException;

import by.epam.felix.kam.project6.command.ICommand;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
/**
 * Views start page for entering name and password.
 * 
 * @author Felix
 *
 */
public class CommandIndexPage implements ICommand {

	public static Logger logger = Logger.getLogger(CommandViewPage.class
			.getName());
	private HttpServletRequest request;
	private HttpServletResponse response;

	public CommandIndexPage(HttpServletRequest request,
			HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	@Override
	public void execute() {
		HttpSession session = request.getSession(false);
		if (null != session) {
			//session.invalidate();

			session.removeAttribute("userInfo");
		}
		RequestDispatcher requestDispatcher = request
				.getRequestDispatcher("index.jsp");
		try {
			requestDispatcher.forward(request, response);
		} catch (ServletException | IOException e) {
			logger.error(e);
		}
	}
}
