package by.epam.felix.kam.project6.command.commands;

import by.epam.felix.kam.project6.command.ICommand;
import by.epam.felix.kam.project6.order.Order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
/**
 * Removes Ingredient from Drink.
 * 
 * @author Felix
 *
 */
public class CommandRemoveIngredient implements ICommand {

	private HttpServletRequest request;
	private HttpServletResponse response;

	public static Logger logger = Logger.getLogger(CommandRemoveIngredient.class.getName());

	public CommandRemoveIngredient(HttpServletRequest request,
			HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	@Override
	public void execute() {
		HttpSession session = request.getSession(false);
		Order order = (Order) session.getAttribute("order");
		int drinkN = Integer.parseInt(request.getParameter("drinkN"));
		int ingredientN = Integer.parseInt(request.getParameter("ingredientN"));
		logger.info("remove ingredient " + drinkN + " " + ingredientN);
		order.getDrink(drinkN - 1).removeIngredient(ingredientN - 1);

		new CommandViewPage(request, response).execute();

	}
}
