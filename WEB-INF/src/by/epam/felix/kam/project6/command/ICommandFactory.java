package by.epam.felix.kam.project6.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class ICommandFactory {
	private static ICommandFactory factory;

	public static void init(String name) {
		if (null == factory) {
			if (name.equals("CommandFactory")) {
				factory = new CommandFactory();
			}
		}
	}

	public static ICommandFactory getFactory() {
		return factory;
	}
	
	/**
	 * Creates command from request.
	 * 
	 * @param request
	 * @param response
	 * @return
	 */
	public abstract ICommand getCommand(HttpServletRequest request,
			HttpServletResponse response);
	
	
	/**
	 * Creates command directly by name. Uses attributes from request if needed.
	 * 
	 * @param request
	 * @param response
	 * @param name String - command name
	 * @return
	 */
	public abstract ICommand getCommand(HttpServletRequest request,
			HttpServletResponse response, ICommandNames name);
}
