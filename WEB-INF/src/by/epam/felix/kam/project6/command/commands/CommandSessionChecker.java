package by.epam.felix.kam.project6.command.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import by.epam.felix.kam.project6.Project6;
import by.epam.felix.kam.project6.command.ICommand;
import by.epam.felix.kam.project6.command.ICommandFactory;
import by.epam.felix.kam.project6.command.ICommandNames;
import by.epam.felix.kam.project6.data.DataFactory1;
import by.epam.felix.kam.project6.initializer.Initializer;
import by.epam.felix.kam.project6.myexception.DataInterfaceException;
import by.epam.felix.kam.project6.myexception.ICommandException;
import by.epam.felix.kam.project6.order.Order;
import by.epam.felix.kam.project6.userinfo.UserInfo;
/**
 * Checks session validation and login in
 * 
 * @author Felix
 *
 */
public class CommandSessionChecker implements ICommand {
	public static Logger logger = Logger.getLogger(CommandSessionChecker.class
			.getName());
	private HttpServletRequest request;
	private HttpServletResponse response;

	public CommandSessionChecker(HttpServletRequest request,
			HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	@Override
	public void execute() throws ICommandException {
		HttpSession session;
		session = request.getSession(true);
		try {
			
			
			if (session.getAttribute("lng")==null){
				String mainFilePath = Project6.link().getServletContext().getRealPath("/")+"\\"
				          +Project6.link().getServletContext().getInitParameter("mainPropertyFile");
				Initializer i = new Initializer(mainFilePath);
				String lng=i.getValue("lang");
				new CommandLangSet(request, response,lng).execute();
				session.setAttribute("lng", lng);
			}
			
			String command=request.getParameter("command").toUpperCase();
			
			if (command.equals("LANGCHANGE")) {
				ICommandFactory
				.getFactory()
				.getCommand(request, response,
						ICommandNames.LANGCHANGE).execute();
				return;
			}
			
			if (command.equals("REGISTER")) {
				session.removeAttribute("userInfo");
				String name=request.getParameter("name");
				String user=request.getParameter("user");
				String password=request.getParameter("password");
				String password2=request.getParameter("password2");
				String account_N=request.getParameter("account_N");
				String message="";
				if (name.equals("")){
					message="Empty name.";
				}
				if (name.length()>45){
					message="Too long name.";
				}
				if (user.equals("")){
					message="Empty username.";
				}
				if (user.length()>15){
					message="Too long username.";
				}
				if (password.equals("")){
					message="Empty password.";
				}
				if (password.length()>15){
					message="Too long password.";
				}
				if (password2.equals("")){
					message="Empty password check.";
				}
				if (!password.equals(password2)){
					message="Error password check.";
				}
				if (account_N.equals("")){
					message="Empty account.";
				}
				if (account_N.length() < 12 || account_N.length() > 20){
					message="Wrong account length";
				}
				if (!message.isEmpty()){
					request.setAttribute("name",name);
					request.setAttribute("password",password);
					request.setAttribute("password2",password2);
					request.setAttribute("user",user);
					request.setAttribute("account_N",account_N);
					request.setAttribute("message",message);
					ICommandFactory
					.getFactory()
					.getCommand(request, response,
							ICommandNames.REGISTERPAGE).execute();
					return;
				} else {
					DataFactory1.getDataFactory1().getDataSource().Register(name, user, password, account_N);
				}
			}
			
			
			
			if (command.equals("LOGIN")||command.equals("REGISTER")) {

				session.removeAttribute("userInfo");
				
				String user = request.getParameter("user");
				String password = request.getParameter("password");
				UserInfo userInfo = DataFactory1.getDataFactory1()
						.getDataSource().getUser(user, password);
				logger.info(user);
				logger.info(password);
				if (null == userInfo) {
					request.setAttribute("message",
							"Invalid username or password!");
					ICommandFactory
							.getFactory()
							.getCommand(request, response,
									ICommandNames.INDEXPAGE).execute();
					return;
				} else {
					//session = request.getSession(false);
					//if (null != session) {
					//	session.invalidate();
					//}
					//session = request.getSession();
					session.setAttribute("userInfo", userInfo);
					
					
					
					
					
					if (userInfo.getAccess_level_Id() == 0) {
						Order order = new Order();
						session.setAttribute("order", order);
						session.setAttribute("money", DataFactory1
								.getDataFactory1().getDataSource()
								.getAccountsRest(userInfo.getId()));
						ICommandFactory
								.getFactory()
								.getCommand(request, response,
										ICommandNames.VIEWPAGE).execute();
					} else {
						ICommandFactory
								.getFactory()
								.getCommand(request, response,
										ICommandNames.EDITPAGE).execute();
					}
					return;
				}
			}
		} catch (DataInterfaceException e) {
			throw new ICommandException(e);
		}

		//session = request.getSession();
		UserInfo userInfo = (UserInfo) session.getAttribute("userInfo");

		if (null == userInfo) {
			ICommandFactory.getFactory()
					.getCommand(request, response, ICommandNames.INDEXPAGE)
					.execute();
		} else {
			ICommandFactory.getFactory().getCommand(request, response)
					.execute();
		}
	}
}