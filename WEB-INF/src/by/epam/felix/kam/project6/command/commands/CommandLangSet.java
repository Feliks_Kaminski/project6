package by.epam.felix.kam.project6.command.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.epam.felix.kam.project6.Project6;
import by.epam.felix.kam.project6.command.ICommand;
import by.epam.felix.kam.project6.initializer.Initializer;
import by.epam.felix.kam.project6.myexception.ICommandException;
/**
 * Loads language properties from file.
 * 
 * @author Felix
 *
 */
public class CommandLangSet implements ICommand{
	private String file;
	private HttpServletRequest request;
	//private HttpServletResponse response;
	
	
	public CommandLangSet(HttpServletRequest request,HttpServletResponse response){
		this.request = request;
		//this.response = response;
		file=request.getParameter("lang");
	}
	
	public CommandLangSet(HttpServletRequest request,HttpServletResponse response,String langFile){
		this.request = request;
		//this.response = response;
		file=langFile;
	}
	
	@Override
	public void execute() throws ICommandException {
			if (file==null){
				file="EN";
			}
			
			Initializer i = new Initializer(Project6.link().getServletContext().getRealPath("/")+"\\WEB-INF\\lang\\"+file+".properties");
			HttpSession session = request.getSession(false);
			session.setAttribute("_Hi", i.getValue("Hi"));
			session.setAttribute("_Logoff", i.getValue("Logoff"));
			session.setAttribute("_Your_money", i.getValue("Your_money"));
			session.setAttribute("_Drinks", i.getValue("Drinks"));
			session.setAttribute("_Ingredients", i.getValue("Ingredients"));
			session.setAttribute("_Name", i.getValue("Name"));
			session.setAttribute("_Price", i.getValue("Price"));
			session.setAttribute("_Portions", i.getValue("Portions"));
			session.setAttribute("_Order", i.getValue("Order"));
			session.setAttribute("_Clear", i.getValue("Clear"));
			session.setAttribute("_Buy", i.getValue("Buy"));
			session.setAttribute("_Total_cost", i.getValue("Total_cost"));
			session.setAttribute("_Remove", i.getValue("Remove"));
			session.setAttribute("_No_money", i.getValue("No_money"));
			session.setAttribute("_Order_empty", i.getValue("Order_empty"));
			session.setAttribute("_Thank_you", i.getValue("Thank_you"));
			session.setAttribute("_Bye", i.getValue("Bye"));
			session.setAttribute("_Not_enough", i.getValue("Not_enough"));
			session.setAttribute("_Ingredients_in_drinks", i.getValue("Ingredients_in_drinks"));

			session.setAttribute("_Username", i.getValue("Username"));
			session.setAttribute("_Password", i.getValue("Password"));
			session.setAttribute("_Password_again", i.getValue("Password_again"));
			session.setAttribute("_Account_N", i.getValue("Account_N"));

			session.setAttribute("_Login", i.getValue("Login"));
			session.setAttribute("_Home", i.getValue("Home"));
			session.setAttribute("_Register", i.getValue("Register"));
	}
}
