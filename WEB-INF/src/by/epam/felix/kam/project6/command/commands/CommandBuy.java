package by.epam.felix.kam.project6.command.commands;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import by.epam.felix.kam.project6.Project6;
import by.epam.felix.kam.project6.command.ICommand;
import by.epam.felix.kam.project6.command.ICommandFactory;
import by.epam.felix.kam.project6.command.ICommandNames;
import by.epam.felix.kam.project6.data.DataFactory1;
import by.epam.felix.kam.project6.myexception.DataInterfaceException;
import by.epam.felix.kam.project6.myexception.ICommandException;
import by.epam.felix.kam.project6.order.Drink;
import by.epam.felix.kam.project6.order.Ingredient;
import by.epam.felix.kam.project6.order.Order;
import by.epam.felix.kam.project6.userinfo.UserInfo;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

/**
 * Makes revision of the Order and sends it to accept in datasource.
 * 
 * @author Felix
 *
 */

public class CommandBuy implements ICommand {

	public static Logger logger = Logger.getLogger(CommandBuy.class.getName());
	private HttpServletRequest request;
	private HttpServletResponse response;

	public CommandBuy(HttpServletRequest request, HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	@Override
	public void execute() throws ICommandException {

		HttpSession session = request.getSession(false);
		Order order = (Order) session.getAttribute("order");
		double totalCost = 0;
		String name;
		Integer count;
		Map<String, Integer> drinks = new HashMap<String, Integer>();
		Map<String, Integer> ingredients = new HashMap<String, Integer>();
		for (Drink drink : order.getDrinks()) {
			totalCost += drink.getPrice();

			name = drink.getName();
			count = drinks.get(name);
			if (null == count) {
				drinks.put(name, 1);
			} else {
				drinks.put(name, ++count);
			}
			for (Ingredient ingredient : drink.getIngredients()) {
				totalCost += ingredient.getPrice();
				name = ingredient.getName();
				count = ingredients.get(name);
				if (null == count) {
					ingredients.put(name, 1);
				} else {
					ingredients.put(name, ++count);
				}
			}
		}

		String warn = null;

		for (Drink d : (Set<Drink>) Project6.link().getServletContext()
				.getAttribute("drinks")) {
			if ((drinks.get(d.getName()) != null)
					&& (d.getCount() < drinks.get(d.getName()))) {
				warn = session.getAttribute("_Not_enough") + " " + d.getName() + "!";
				break;
			}
		}
		for (Ingredient i : (Set<Ingredient>) Project6.link()
				.getServletContext().getAttribute("ingredients")) {
			if ((ingredients.get(i.getName()) != null)
					&& (i.getCount() < ingredients.get(i.getName()))) {
				warn = session.getAttribute("_Not_enough") + " " + i.getName() + "!";
				break;
			}
		}

		if (null != warn) {
			request.setAttribute("message", warn);
		} else {
			if ((Double) session.getAttribute("money") < totalCost) {
				request.setAttribute("message", session.getAttribute("_No_money"));
			} else {
				if (0 == totalCost) {
					request.setAttribute("message",  session.getAttribute("_Order_empty"));
				} else {
					int userId = ((UserInfo) session.getAttribute("userInfo"))
							.getId();
					try {
						DataFactory1.getDataFactory1().getDataSource()
								.acceptOrder(userId, order);

						request.setAttribute("message", session.getAttribute("_Thank_you"));

						session.setAttribute("money", DataFactory1
								.getDataFactory1().getDataSource()
								.getAccountsRest(userId));
					} catch (DataInterfaceException e) {
						throw new ICommandException(e);
					}
					ICommandFactory
							.getFactory()
							.getCommand(
									null,
									null,
									ICommandNames.REFRESHMEMO).execute();
					order.clearDrinks();
				}
			}
		}
		new CommandViewPage(request, response).execute();
	}

}
