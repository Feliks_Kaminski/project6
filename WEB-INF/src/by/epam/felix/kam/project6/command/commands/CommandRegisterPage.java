package by.epam.felix.kam.project6.command.commands;

import java.io.IOException;

import by.epam.felix.kam.project6.command.ICommand;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
/**
 * Views Order forming page.
 * 
 * @author Felix
 *
 */
public class CommandRegisterPage implements ICommand {

	public static Logger logger = Logger.getLogger(CommandRegisterPage.class.getName());
	private HttpServletRequest request;
	private HttpServletResponse response;

	public CommandRegisterPage(HttpServletRequest request,
			HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	@Override
	public void execute() {
		RequestDispatcher requestDispatcher = request
				.getRequestDispatcher("register.jsp");
		try {
			requestDispatcher.forward(request, response);
		} catch (ServletException | IOException e) {
			logger.error(e);
		}
	}

}
