package by.epam.felix.kam.project6.command.commands;

import by.epam.felix.kam.project6.command.ICommand;
import by.epam.felix.kam.project6.command.ICommandFactory;
import by.epam.felix.kam.project6.command.ICommandNames;
import by.epam.felix.kam.project6.data.DataFactory1;
import by.epam.felix.kam.project6.myexception.DataInterfaceException;
import by.epam.felix.kam.project6.myexception.ICommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
/**
 * Changes price or count of the Ingredient.
 * 
 * @author Felix
 *
 */
public class CommandEditIngredient implements ICommand {

	public static Logger logger = Logger.getLogger(CommandEditIngredient.class
			.getName());
	private HttpServletRequest request;
	private HttpServletResponse response;

	public CommandEditIngredient(HttpServletRequest request,
			HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	@Override
	public void execute() throws ICommandException {
		try {
			DataFactory1
					.getDataFactory1()
					.getDataSource()
					.editIngredient(
							Integer.parseInt(request
									.getParameter("ingredientId")),
							Integer.parseInt(request.getParameter("newcount")),
							Double.parseDouble(request.getParameter("newprice")));
			ICommandFactory.getFactory()
					.getCommand(null, null, ICommandNames.REFRESHMEMO)
					.execute();
			new CommandEditPage(request, response).execute();
		} catch (DataInterfaceException e) {
			throw new ICommandException(e);
		}
	}
}
