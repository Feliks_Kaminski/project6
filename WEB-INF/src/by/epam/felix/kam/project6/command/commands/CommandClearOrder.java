package by.epam.felix.kam.project6.command.commands;

import by.epam.felix.kam.project6.command.ICommand;
import by.epam.felix.kam.project6.order.Order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
/**
 * Makes the Order empty.
 * 
 * @author Felix
 *
 */
public class CommandClearOrder implements ICommand {

	private HttpServletRequest request;
	private HttpServletResponse response;

	public static Logger logger = Logger.getLogger(CommandClearOrder.class
			.getName());

	public CommandClearOrder(HttpServletRequest request,
			HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	@Override
	public void execute() {
		HttpSession session = request.getSession(false);
		Order order = (Order) session.getAttribute("order");
		logger.info("clearDrinks");
		order.clearDrinks();
		;

		new CommandViewPage(request, response).execute();

	}
}
