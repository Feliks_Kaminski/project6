package by.epam.felix.kam.project6.command.commands;

import by.epam.felix.kam.project6.command.ICommand;
import by.epam.felix.kam.project6.command.ICommandFactory;
import by.epam.felix.kam.project6.command.ICommandNames;
import by.epam.felix.kam.project6.data.DataFactory1;
import by.epam.felix.kam.project6.myexception.DataInterfaceException;
import by.epam.felix.kam.project6.myexception.ICommandException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
/**
 * Changes price or count of the Drink.
 * 
 * @author Felix
 *
 */
public class CommandEditDrink implements ICommand {

	public static Logger logger = Logger.getLogger(CommandEditDrink.class
			.getName());
	private HttpServletRequest request;
	private HttpServletResponse response;

	public CommandEditDrink(HttpServletRequest request,
			HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	@Override
	public void execute() throws ICommandException {
		try {
			DataFactory1
					.getDataFactory1()
					.getDataSource()
					.editDrink(
							Integer.parseInt(request.getParameter("drinkId")),
							Integer.parseInt(request.getParameter("newcount")),
							Double.parseDouble(request.getParameter("newprice")));
			ICommandFactory.getFactory()
					.getCommand(null, null, ICommandNames.REFRESHMEMO)
					.execute();
			new CommandEditPage(request, response).execute();
		} catch (DataInterfaceException e) {
			throw new ICommandException(e);
		}
	}
}
