package by.epam.felix.kam.project6.command.commands;

import by.epam.felix.kam.project6.command.ICommand;
import by.epam.felix.kam.project6.order.Drink;
import by.epam.felix.kam.project6.order.Ingredient;
import by.epam.felix.kam.project6.order.Order;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
/**
 * Calculates total cost of the order and sends it into session.
 * 
 * @author Felix
 *
 */
public class CommandCalculateCost implements ICommand {

	private HttpServletRequest request;
	// private HttpServletResponse response;

	public static Logger logger = Logger.getLogger(CommandCalculateCost.class
			.getName());

	public CommandCalculateCost(HttpServletRequest request,
			HttpServletResponse response) {
		this.request = request;
		// this.response=response;
	}

	@Override
	public void execute() {
		HttpSession session = request.getSession(false);
		Order order = (Order) session.getAttribute("order");
		double totalCost = 0;
		for (Drink drink : order.getDrinks()) {
			totalCost += drink.getPrice();
			for (Ingredient ingredient : drink.getIngredients()) {
				totalCost += ingredient.getPrice();
			}
		}

		request.setAttribute("totalCost", totalCost);

	}
}