package by.epam.felix.kam.project6.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.felix.kam.project6.command.commands.*;

public class CommandFactory extends ICommandFactory {

	CommandFactory() {

	}

	@Override
	public ICommand getCommand(HttpServletRequest request,
			HttpServletResponse response) {
		return getCommand(request, response, ICommandNames.valueOf(request
				.getParameter("command").toUpperCase()));
	}

	@Override
	public ICommand getCommand(HttpServletRequest request,
			HttpServletResponse response, ICommandNames name) {
		switch (name) {
		case LOGOFF:
			return new CommandLogoff(request, response);
		case ADDDRINK:
			return new CommandAddDrink(request, response);
		case VIEWPAGE:
			return new CommandViewPage(request, response);
		case INDEXPAGE:
			return new CommandIndexPage(request, response);
		case EDITPAGE:
			return new CommandEditPage(request, response);
		case ADDINGREDIENT:
			return new CommandAddIngredient(request, response);
		case CLEARORDER:
			return new CommandClearOrder(request, response);
		case REMOVEDRINK:
			return new CommandRemoveDrink(request, response);
		case REMOVEINGREDIENT:
			return new CommandRemoveIngredient(request, response);
		case BUY:
			return new CommandBuy(request, response);
		case INIT1:
			return new CommandInit1();
		case REFRESHMEMO:
			return new CommandRefreshMemo();
		case CALCULATECOST:
			return new CommandCalculateCost(request, response);
		case EDITDRINK:
			return new CommandEditDrink(request, response);
		case EDITINGREDIENT:
			return new CommandEditIngredient(request, response);
		case SESSIONCHECKER:
			return new CommandSessionChecker(request, response);
		case LANGCHANGE:
			return new CommandLangChange(request, response);
		case REGISTERPAGE:
			return new CommandRegisterPage(request, response);
		default:
			return null;
		}
	}

}
