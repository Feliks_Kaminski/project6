package by.epam.felix.kam.project6.command.commands;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.epam.felix.kam.project6.command.ICommand;
import by.epam.felix.kam.project6.command.ICommandFactory;
import by.epam.felix.kam.project6.command.ICommandNames;
import by.epam.felix.kam.project6.myexception.ICommandException;
/**
 * Calls LangSet command and ViewPage.
 * 
 * @author Felix
 *
 */
public class CommandLangChange implements ICommand{
	private String file;
	private HttpServletRequest request;
	private HttpServletResponse response;

	public static Logger logger = Logger.getLogger(CommandLangChange.class.getName());
	
	public CommandLangChange(HttpServletRequest request,HttpServletResponse response){
		this.request = request;
		this.response = response;
		file=request.getParameter("lang");
	}
	
	public CommandLangChange(HttpServletRequest request,HttpServletResponse response,String langFile){
		this.request = request;
		this.response = response;
		file=langFile;
	}
	
	@Override
	public void execute() throws ICommandException {
		new CommandLangSet(request, response,file).execute();
		String page=request.getParameter("page").toUpperCase();
		logger.info(page);
		ICommandFactory.getFactory().getCommand(request, response,ICommandNames.valueOf(page)).execute();
	}
}
