package by.epam.felix.kam.project6.command.commands;

import java.io.IOException;

import by.epam.felix.kam.project6.Project6;
import by.epam.felix.kam.project6.command.ICommand;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
/**
 * Views page for editing Drinks and Ingredients.
 * 
 * @author Felix
 *
 */
public class CommandEditPage implements ICommand {
	public static Logger logger = Logger.getLogger(CommandViewPage.class
			.getName());
	private HttpServletRequest request;
	private HttpServletResponse response;

	public CommandEditPage(HttpServletRequest request,
			HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	@Override
	public void execute() {
		request.setAttribute("ingredients", Project6.link().getServletContext()
				.getAttribute("ingredients"));
		request.setAttribute("drinks", Project6.link().getServletContext()
				.getAttribute("drinks"));
		RequestDispatcher requestDispatcher = request
				.getRequestDispatcher("/WEB-INF/jsp/edit.jsp");
		try {
			requestDispatcher.forward(request, response);
		} catch (ServletException | IOException e) {
			logger.error(e);
		}
	}

}