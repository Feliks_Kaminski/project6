package by.epam.felix.kam.project6.command.commands;

import java.io.IOException;

import by.epam.felix.kam.project6.command.ICommand;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
/**
 * Invalidates session and views start page.
 * 
 * @author Felix
 *
 */
public class CommandLogoff implements ICommand {
	public static Logger logger = Logger.getLogger(CommandLogoff.class.getName());
	private HttpServletRequest request;
	private HttpServletResponse response;

	public CommandLogoff(HttpServletRequest request,
			HttpServletResponse response) {
		this.request = request;
		this.response = response;
	}

	@Override
	public void execute() {
		HttpSession session=request.getSession(false);

		request.setAttribute("message", session.getAttribute("_Bye"));

		//session.invalidate();
		session.removeAttribute("userInfo");
		RequestDispatcher requestDispatcher = request
				.getRequestDispatcher("index.jsp");
		try {
			requestDispatcher.forward(request, response);
		} catch (ServletException | IOException e) {
			logger.error(e);
		}
	}

}
