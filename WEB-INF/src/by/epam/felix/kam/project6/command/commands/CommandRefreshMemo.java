package by.epam.felix.kam.project6.command.commands;

import by.epam.felix.kam.project6.Project6;
import by.epam.felix.kam.project6.command.ICommand;
import by.epam.felix.kam.project6.data.DataFactory1;
import by.epam.felix.kam.project6.myexception.DataInterfaceException;
import by.epam.felix.kam.project6.myexception.ICommandException;
/**
 * Reloads rests from datasouce into context.
 * 
 * @author Felix
 *
 */
public class CommandRefreshMemo implements ICommand {

	@Override
	public void execute() throws ICommandException {
		try {
			Project6.link()
					.getServletContext()
					.setAttribute(
							"drinks",
							DataFactory1.getDataFactory1().getDataSource()
									.getDrinks());
			Project6.link()
					.getServletContext()
					.setAttribute(
							"ingredients",
							DataFactory1.getDataFactory1().getDataSource()
									.getIngredients());
			// Memo.setDrinks(DataFactory1.getDataFactory1().getDataSource().getDrinks());
			// Memo.setIngredients(DataFactory1.getDataFactory1().getDataSource().getIngredients());
		} catch (DataInterfaceException e) {
			throw new ICommandException(e);
		}
	}

}
