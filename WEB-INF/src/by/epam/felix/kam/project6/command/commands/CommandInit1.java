package by.epam.felix.kam.project6.command.commands;

import org.apache.log4j.Logger;

import by.epam.felix.kam.project6.Project6;
import by.epam.felix.kam.project6.command.ICommand;
import by.epam.felix.kam.project6.command.ICommandFactory;
import by.epam.felix.kam.project6.command.ICommandNames;
import by.epam.felix.kam.project6.data.DataFactory1;
import by.epam.felix.kam.project6.initializer.Initializer;
import by.epam.felix.kam.project6.myexception.DataInterfaceException;
import by.epam.felix.kam.project6.myexception.ICommandException;
/**
 * Initilization command.
 * 
 * @author Felix
 *
 */
public class CommandInit1 implements ICommand {

	public static Logger logger = Logger.getLogger(CommandInit1.class.getName());
	@Override
	public void execute() throws ICommandException {
		
		String filePath = Project6.link().getServletContext().getRealPath("/")+"\\"
				+Project6.link().getServletContext().getInitParameter("mainPropertyFile");
		logger.info(filePath);
		Initializer i = new Initializer(filePath);
		String factoryName = i.getValue("CommandFactory");
		String dataSourceName = i.getValue("DataSource");
		logger.info(factoryName);
		try {
			ICommandFactory.init(factoryName);
			DataFactory1.getDataFactory1().init(dataSourceName);
		} catch (DataInterfaceException e) {
			throw new ICommandException(e);
		}
		ICommandFactory.getFactory().getCommand(null, null, ICommandNames.REFRESHMEMO).execute();
	}
}
