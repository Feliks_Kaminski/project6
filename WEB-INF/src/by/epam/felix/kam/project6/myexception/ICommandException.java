package by.epam.felix.kam.project6.myexception;

public class ICommandException extends MyException{

	public ICommandException() {
		super();
	}

	public ICommandException(String message, Throwable cause) {
		super(message, cause);
	}

	public ICommandException(String message) {
		super(message);
	}

	public ICommandException(Throwable cause) {
		super(cause);
	}
}