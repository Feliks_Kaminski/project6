package by.epam.felix.kam.project6.myexception;

public class WrongConnectionException extends MyException {

	public WrongConnectionException() {
		super();
	}

	public WrongConnectionException(String message, Throwable cause) {
		super(message, cause);
	}

	public WrongConnectionException(String message) {
		super(message);
	}

	public WrongConnectionException(Throwable cause) {
		super(cause);
	}

}
