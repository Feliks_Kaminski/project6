package by.epam.felix.kam.project6.myexception;

public class ConnectionCreateException extends MyException {

	public ConnectionCreateException() {
		super();
	}

	public ConnectionCreateException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConnectionCreateException(String message) {
		super(message);
	}

	public ConnectionCreateException(Throwable cause) {
		super(cause);
	}

}