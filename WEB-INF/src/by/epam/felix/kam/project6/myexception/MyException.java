package by.epam.felix.kam.project6.myexception;

public abstract class MyException extends Exception {
	public MyException() {

	}

	public MyException(String message, Throwable cause) {
		super(message, cause);
	}

	public MyException(String message) {
		super(message);
	}

	public MyException(Throwable cause) {
		super(cause);
	}
}
