package by.epam.felix.kam.project6.myexception;

public class InitializerException extends MyException{

	public InitializerException() {
		super();
	}

	public InitializerException(String message, Throwable cause) {
		super(message, cause);
	}

	public InitializerException(String message) {
		super(message);
	}

	public InitializerException(Throwable cause) {
		super(cause);
	}
	
}
