package by.epam.felix.kam.project6.myexception;

public class DataInterfaceException extends MyException {

	public DataInterfaceException() {
		super();
	}

	public DataInterfaceException(String message, Throwable cause) {
		super(message, cause);
	}

	public DataInterfaceException(String message) {
		super(message);
	}

	public DataInterfaceException(Throwable cause) {
		super(cause);
	}

}