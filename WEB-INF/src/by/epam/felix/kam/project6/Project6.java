package by.epam.felix.kam.project6;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.LogManager;
import org.apache.log4j.xml.DOMConfigurator;
import org.apache.log4j.Logger;

import by.epam.felix.kam.project6.command.ICommandFactory;
import by.epam.felix.kam.project6.command.ICommandNames;
import by.epam.felix.kam.project6.command.commands.CommandInit1;
import by.epam.felix.kam.project6.myexception.ICommandException;

/**
 * Main Servlet.
 *
 *
 * @author Felix
 */
public class Project6 extends HttpServlet {
	private static Project6 link;
	public static Logger logger = Logger.getLogger(Project6.class.getName());

	public Project6() {

	}

	public static Project6 link() {
		return link;
	}

	@Override
	public void init() {
		link = this;
		new DOMConfigurator().doConfigure( Project6.link().getServletContext().getRealPath("/")+
				"\\WEB-INF\\log4j.xml",
				LogManager.getLoggerRepository());
		try {
			new CommandInit1().execute();
		} catch (Exception e) {
			logger.error("InitFail", e);
		}

	}

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		go(request, response);
	}

	private void go(HttpServletRequest request, HttpServletResponse response) {
		try {
			ICommandFactory.getFactory().getCommand(request, response, ICommandNames.SESSIONCHECKER).execute();
		} catch (ICommandException | RuntimeException e) {
			logger.error("Error in main", e);
			request.setAttribute("message", "Servlet Error!");
			try {
				ICommandFactory.getFactory().getCommand(request, response, ICommandNames.INDEXPAGE).execute();
			} catch (Exception ex) {
				logger.error("SuperError in main", ex);
			}
		}
	}
}
