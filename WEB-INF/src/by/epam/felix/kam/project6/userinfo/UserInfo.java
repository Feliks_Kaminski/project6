package by.epam.felix.kam.project6.userinfo;

/**
 * Holds user information.
 * 
 * @author Felix
 */
public class UserInfo {
	private final String user;
	private final String name;
	private final int user_Id;
	private final int access_Level_Id;
	
	public UserInfo(String user, String name, int user_Id, int access_Level_Id) {
		this.user = user;
		this.name = name;
		this.user_Id = user_Id;
		this.access_Level_Id = access_Level_Id;
	}

	public int getId() {
		return user_Id;
	}

	public String getUser() {
		return user;
	}

	public String getName() {
		return name;
	}

	public int getAccess_level_Id() {
		return access_Level_Id;
	}
}
