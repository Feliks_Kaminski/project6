package by.epam.felix.kam.project6.order;

/**
 * Ingredient.
 * 
 * Ingredients puts in Drinks.
 * 
 * @author Felix
 */
public class Ingredient {
	private final String name;
	private int count;
	private double price;
	private final int ingredientId;
	
	
	
	public Ingredient(String name, int id) {
		this.name = name;
		this.ingredientId = id;
	}
	
	/**
	 * Returns ingredients id in database.
	 * 
	 */ 
	
	public int getIngredientId() {
		return ingredientId;
	}

	public String getName() {
		return name;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public int hashCode() {
		return name.hashCode();
	}
}
