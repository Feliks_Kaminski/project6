package by.epam.felix.kam.project6.order;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds list of selected Drinks.
 * 
 * @author Felix
 */
public class Order {
	private List<Drink> drinks = new ArrayList<Drink>();

	/**
	 * Removes Drink with all his Ingredients from Order.
	 * 
	 * @param index
	 *            int - N of the Drink.
	 */

	public void removeDrink(int index) {
		drinks.remove(index);
	}

	/**
	 * Returns number of selected Drinks in Order.
	 * 
	 * @return int - number of Drinks
	 */
	public int size() {
		return drinks.size();
	}

	/**
	 * Adds Drink to Drink list of Order.
	 * 
	 * @param drink
	 *            Drink
	 */
	public void addDrink(Drink drink) {
		drinks.add(drink);
	}

	/**
	 * Gets Drink in desired position.
	 * 
	 * @param index
	 * @return Drink
	 */
	public Drink getDrink(int index) {
		return drinks.get(index);
	}

	/**
	 * Gets Drink list.
	 * 
	 * @return List<Drink>
	 */

	public List<Drink> getDrinks() {
		return drinks;
	}

	/**
	 * Removes all Drinks from Order.
	 * 
	 */

	public void clearDrinks() {
		drinks.clear();
		;
	}
}
