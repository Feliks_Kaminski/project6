package by.epam.felix.kam.project6.order;

import java.util.ArrayList;
import java.util.List;

/**
 * Holds list of selected Ingredients for current Drink.
 * 
 * @author Felix
 */
public class Drink {
	private final String name;
	private int count;
	private double price;
	private final int drinkId;
	List<Ingredient> ingredients = new ArrayList<Ingredient>();

	public Drink(String name, int id) {
		this.name = name;
		this.drinkId = id;
	}

	public String getName() {
		return name;
	}

	public int getDrinkId() {
		return drinkId;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	/**
	 * Removes Ingredient from Drink.
	 * 
	 * @param index
	 *            int - N of the Ingredient.
	 */
	public void removeIngredient(int index) {
		ingredients.remove(index);
	}

	/**
	 * Adds Ingredient to Drink.
	 * 
	 * @param ingredient
	 *            Ingredient
	 */
	public void addIngredient(Ingredient ingredient) {
		ingredients.add(ingredient);
	}

	/**
	 * Returns number of selected Ingredients in Drink.
	 * 
	 * @return int - number of Ingredients
	 */
	public int size() {
		return ingredients.size();
	}

	/**
	 * Gets Ingredient in desired position.
	 * 
	 * @param index
	 * @return Ingredient
	 */
	public Ingredient getIngredient(int index) {
		return ingredients.get(index);
	}

	/**
	 * Gets Ingredient list.
	 * 
	 * @return List<Ingredient>
	 */
	public List<Ingredient> getIngredients() {
		return ingredients;
	}

	public int hashCode() {
		return name.hashCode();
	}
}
