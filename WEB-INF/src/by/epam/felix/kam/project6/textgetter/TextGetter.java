package by.epam.felix.kam.project6.textgetter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import org.apache.log4j.Logger;

/**
 * Reads text content of the file.
 * 
 * @author Felix
 */
public class TextGetter {
	public static Logger logger = Logger.getLogger(TextGetter.class.getName());

	private TextGetter() {

	}

	/**
	 * Gets file content.
	 * 
	 * @param fileName
	 *            String
	 * @return String - file content (text)
	 * @throws IOException
	 */

	public static String getText(String fileName) throws IOException {
		File file = new File(fileName);
		StringBuilder sb = new StringBuilder();
		if (file.exists() & file.isFile()) {
			BufferedReader br = null;
			try {
				br = new BufferedReader(new FileReader(file));
				while (br.ready()) {
					sb.append((char) br.read());
				}
			} catch (IOException ex) {
				logger.error("Reading file error.");
				throw ex;
			} finally {
				try {
					if (br != null) {
						br.close();
					}
				} catch (IOException ex) {
					logger.error("Closing file error.");
					throw ex;
				}
			}
		} else {
			logger.error("No file." + fileName);
			throw new FileNotFoundException("No file.");
		}
		return sb.toString();
	}
}
