package by.epam.felix.kam.project6.data;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import by.epam.felix.kam.project6.connectionspool.ConnectionsPool;
import by.epam.felix.kam.project6.connectionspool.Pool;
import by.epam.felix.kam.project6.myexception.ConnectionCreateException;
import by.epam.felix.kam.project6.myexception.DataInterfaceException;
import by.epam.felix.kam.project6.myexception.MyException;
import by.epam.felix.kam.project6.order.Drink;
import by.epam.felix.kam.project6.order.Ingredient;
import by.epam.felix.kam.project6.order.Order;
import by.epam.felix.kam.project6.textgetter.TextGetter;
import by.epam.felix.kam.project6.userinfo.UserInfo;

public class DataDB implements DataInterface {
	private static String selectDrinks;
	private static String selectIngredients;
	private static String selectUser;
	private static String selectAccount;
	private static String insertOrderHead;
	private static String insertOrderDrink;
	private static String insertOrderIngredient;
	private static String selectLastId;
	private static String acceptOrder;
	private static String editDrinks;
	private static String editIngredients;
	private static String register;

	private final Lock editing = new ReentrantLock();

	public static Logger logger = Logger.getLogger(DataDB.class.getName());
	private Pool pool;

	public DataDB(String url,String DBUser,String DBPassword,int DBConnectionsCount,String SQLPath) throws DataInterfaceException {
		try {
			logger.info(SQLPath);
			selectDrinks = TextGetter.getText(SQLPath + "SelectDrinks.sql");
			selectIngredients = TextGetter.getText(SQLPath
					+ "SelectIngredients.sql");
			selectUser = TextGetter.getText(SQLPath + "SelectUser.sql");
			selectAccount = TextGetter.getText(SQLPath + "SelectAccount.sql");
			insertOrderHead = TextGetter.getText(SQLPath + "InsertOrderHead.sql");
			insertOrderDrink = TextGetter.getText(SQLPath + "InsertOrderDrink.sql");
			insertOrderIngredient = TextGetter.getText(SQLPath
					+ "InsertOrderIngredient.sql");
			selectLastId = TextGetter.getText(SQLPath + "SelectLastId.sql");
			acceptOrder = TextGetter.getText(SQLPath + "AcceptOrder.sql");
			editDrinks = TextGetter.getText(SQLPath + "EditDrinks.sql");
			editIngredients = TextGetter.getText(SQLPath + "EditIngredients.sql");
			register = TextGetter.getText(SQLPath + "Register.sql");
			pool = new ConnectionsPool(url,
										DBUser,
										DBPassword,
										DBConnectionsCount);

		} catch (ConnectionCreateException | IOException e) {
			logger.error("DataDB constructor",e);
			throw new DataInterfaceException(e);
		}
	}

	@Override
	public Set<Drink> getDrinks() throws DataInterfaceException {

		Set<Drink> drinks = new HashSet<Drink>();
		try {
			Connection con = pool.getConection();
			try {
				PreparedStatement statement = con
						.prepareStatement(selectDrinks);
				ResultSet answer = statement.executeQuery();

				Drink drink;
				while (answer.next()) {
					drink = new Drink(answer.getString("name"),
							answer.getInt("id_drink"));
					drink.setCount(answer.getInt("count"));
					drink.setPrice(answer.getDouble("cur_cost"));
					drinks.add(drink);
				}
			} finally {
				pool.returnConection(con);
			}
		} catch (MyException | InterruptedException | SQLException e) {
			throw new DataInterfaceException(e);
		}

		return drinks;
	}

	@Override
	public Set<Ingredient> getIngredients() throws DataInterfaceException {

		Set<Ingredient> ingredients = new HashSet<Ingredient>();
		try {
			Connection con = pool.getConection();
			try {
				PreparedStatement statement = con
						.prepareStatement(selectIngredients);
				ResultSet answer = statement.executeQuery();

				Ingredient ingredient;
				while (answer.next()) {
					ingredient = new Ingredient(answer.getString("name"),
							answer.getInt("id_ingredient"));
					ingredient.setCount(answer.getInt("count"));
					ingredient.setPrice(answer.getDouble("cur_cost"));
					ingredients.add(ingredient);
				}
			} finally {
				pool.returnConection(con);
			}
		} catch (MyException | InterruptedException | SQLException e) {
			throw new DataInterfaceException(e);
		}

		return ingredients;
	}

	@Override
	public UserInfo getUser(String user, String password)
			throws DataInterfaceException {
		UserInfo userInfo = null;
		try {
			Connection con = pool.getConection();
			try {
				logger.info(user);
				logger.info(password);
				PreparedStatement statement = con.prepareStatement(selectUser);
				statement.setString(1, user);
				statement.setString(2, password);
				ResultSet answer = statement.executeQuery();

				while (answer.next()) {
					userInfo = new UserInfo(user, answer.getString("name"),
							answer.getInt("id_user"),
							answer.getInt("access_level_id"));
				}
			} finally {
				pool.returnConection(con);
			}
		} catch (MyException | InterruptedException | SQLException e) {
			logger.error("User", e);
			throw new DataInterfaceException(e);
		}
		return userInfo;
	}

	@Override
	public double getAccountsRest(int userId) throws DataInterfaceException {
		double money = 0;
		try {
			Connection con = pool.getConection();
			try {
				PreparedStatement statement = con
						.prepareStatement(selectAccount);
				statement.setInt(1, userId);
				ResultSet answer = statement.executeQuery();

				while (answer.next()) {
					money = answer.getDouble("sum");
				}
			} finally {
				pool.returnConection(con);
			}
		} catch (MyException | InterruptedException | SQLException e) {
			logger.error("AccountRest", e);
			throw new DataInterfaceException(e);
		}
		return money;
	}

	@Override
	public void acceptOrder(int userId, Order order)
			throws DataInterfaceException {
		editing.lock();
		Connection con = null;
		try {
			con = pool.getConection();

			con.setAutoCommit(false);
			PreparedStatement statementInsertOrder = con
					.prepareStatement(insertOrderHead);
			statementInsertOrder.setInt(1, userId);
			statementInsertOrder.execute();

			PreparedStatement statementLastId = con
					.prepareStatement(selectLastId);
			ResultSet answer = statementLastId.executeQuery();

			answer.next();
			int orderId = answer.getInt(1);

			PreparedStatement statementInsertDrink = con
					.prepareStatement(insertOrderDrink);
			PreparedStatement statementInsertIngredient = con
					.prepareStatement(insertOrderIngredient);

			int order_Drink_Id;
			for (Drink drink : order.getDrinks()) {

				statementInsertDrink.setInt(1, orderId);
				statementInsertDrink.setInt(2, drink.getDrinkId());
				statementInsertDrink.execute();
				answer = statementLastId.executeQuery();
				answer.next();
				order_Drink_Id = answer.getInt(1);

				for (Ingredient ingredient : drink.getIngredients()) {
					statementInsertIngredient.setInt(1, order_Drink_Id);
					statementInsertIngredient.setInt(2,
							ingredient.getIngredientId());
					statementInsertIngredient.execute();
				}
			}

			CallableStatement cs = con.prepareCall(acceptOrder);
			cs.setInt(1, orderId);
			cs.execute();
			con.commit();
			pool.returnConection(con);
		} catch (MyException | InterruptedException | SQLException e) {
			logger.error("buy", e);
			try {
				con.rollback();
				pool.returnConection(con);
			} catch (Exception ex) {
				logger.error("buy rollback");
			}
			throw new DataInterfaceException(e);
		} finally {
			editing.unlock();
		}
	}

	@Override
	public void editDrink(int drinkId, int count, double price)
			throws DataInterfaceException {
		editing.lock();
		try {
			Connection con = pool.getConection();
			con.setAutoCommit(true);
			try {
				PreparedStatement statement = con.prepareStatement(editDrinks);
				statement.setDouble(1, price);
				statement.setInt(2, count);
				statement.setInt(3, drinkId);
				statement.execute();
			} finally {
				pool.returnConection(con);
			}
		} catch (MyException | InterruptedException | SQLException e) {
			logger.error("EditDrink", e);
			throw new DataInterfaceException(e);
		} finally {
			editing.unlock();
		}
	}

	@Override
	public void editIngredient(int ingredientId, int count, double price)
			throws DataInterfaceException {
		editing.lock();
		try {
			Connection con = pool.getConection();
			con.setAutoCommit(true);
			try {
				PreparedStatement statement = con.prepareStatement(editIngredients);
				statement.setDouble(1, price);
				statement.setInt(2, count);
				statement.setInt(3, ingredientId);
				statement.execute();
			} finally {
				pool.returnConection(con);
			}
		} catch (MyException | InterruptedException | SQLException e) {
			logger.error("EditIngredient", e);
			throw new DataInterfaceException(e);
		} finally {
			editing.unlock();
		}
	}

	@Override
	public void Register(String name, String user, String password,
			String account_N) throws DataInterfaceException {
		editing.lock();
		Connection con = null;
		try {
			con = pool.getConection();

			con.setAutoCommit(false);
			CallableStatement cs = con.prepareCall(register);
			cs.setString(1, name);
			cs.setString(2, user);
			cs.setString(3, password);
			cs.setString(4, account_N);
			cs.execute();
			con.commit();
		} catch (MyException | InterruptedException | SQLException e) {
			logger.error("Register", e);
			try {
				con.rollback();
			} catch (Exception ex) {
				logger.error("Register rollback");
			}
			throw new DataInterfaceException(e);
		} finally {

			editing.unlock();
			try {
				pool.returnConection(con);
			} catch (Exception ex) {
				logger.error("Register rollback");
			}
		}
	}
}
