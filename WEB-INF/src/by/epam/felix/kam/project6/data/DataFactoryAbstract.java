package by.epam.felix.kam.project6.data;

public abstract class DataFactoryAbstract {
	public abstract DataInterface getDataSource();
}
