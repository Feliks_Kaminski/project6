package by.epam.felix.kam.project6.data;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import by.epam.felix.kam.project6.myexception.DataInterfaceException;
import by.epam.felix.kam.project6.order.Drink;
import by.epam.felix.kam.project6.order.Ingredient;
import by.epam.felix.kam.project6.order.Order;
import by.epam.felix.kam.project6.userinfo.UserInfo;

public class DataHardCode implements DataInterface {
	private double account = 15000d;
	private Set<Drink> drinks = new HashSet<Drink>();
	private Set<Ingredient> ingredients = new HashSet<Ingredient>();

	{
		Drink drink = new Drink("Tea", 1);
		drink.setPrice(2000D);
		drink.setCount(4);
		drinks.add(drink);

		drink = new Drink("Espresso", 2);
		drink.setPrice(3000D);
		drink.setCount(5);
		drinks.add(drink);

		Ingredient ingredient = new Ingredient("Shugar", 1);
		ingredient.setPrice(300D);
		ingredient.setCount(4);
		ingredients.add(ingredient);

		ingredient = new Ingredient("Milk", 2);
		ingredient.setPrice(300D);
		ingredient.setCount(5);
		ingredients.add(ingredient);

		ingredient = new Ingredient("Lemon juice", 3);
		ingredient.setPrice(400D);
		ingredient.setCount(5);
		ingredients.add(ingredient);

		ingredient = new Ingredient("Cream", 4);
		ingredient.setPrice(1000D);
		ingredient.setCount(5);
		ingredients.add(ingredient);
	}

	public Set<Drink> getDrinks() {
		Set<Drink> drinksCopy = new HashSet<Drink>();
		for (Drink d : drinks) {
			Drink newd = new Drink(d.getName(), d.getDrinkId());
			newd.setCount(d.getCount());
			newd.setPrice(d.getPrice());
			drinksCopy.add(newd);
		}
		return drinksCopy;
	}

	public Set<Ingredient> getIngredients() {
		Set<Ingredient> ingredientsCopy = new HashSet<Ingredient>();
		for (Ingredient i : ingredients) {
			Ingredient newi = new Ingredient(i.getName(), i.getIngredientId());
			newi.setCount(i.getCount());
			newi.setPrice(i.getPrice());
			ingredientsCopy.add(newi);
		}
		return ingredientsCopy;
	}

	public UserInfo getUser(String user, String password) {
		if ((user.equals("II")) && (password.equals("1"))) {
			return new UserInfo(user, "Felix", 50, 0);
		}
		if ((user.equals("Sys")) && (password.equals("dba"))) {
			return new UserInfo(user, "Admin", 1, 1);
		}
		return null;
	}

	public double getAccountsRest(int userId) {
		if (50 == userId) {
			return account;
		} else {
			return 0;
		}
	}

	public void acceptOrder(int userId, Order order) {

		double totalCost = 0;
		String name;
		Integer count;
		Map<String, Integer> drinks = new HashMap<String, Integer>();
		Map<String, Integer> ingredients = new HashMap<String, Integer>();
		for (Drink drink : order.getDrinks()) {
			totalCost += drink.getPrice();

			name = drink.getName();
			count = drinks.get(name);
			if (null == count) {
				drinks.put(name, 1);
			} else {
				drinks.put(name, ++count);
			}
			for (Ingredient ingredient : drink.getIngredients()) {
				totalCost += ingredient.getPrice();
				name = ingredient.getName();
				count = ingredients.get(name);
				if (null == count) {
					ingredients.put(name, 1);
				} else {
					ingredients.put(name, ++count);
				}
			}
		}

		account -= totalCost;

		// Iterator<Map.Entry<String, Integer>> i=drinks.entrySet().iterator();

		// while(i.hasNext())

		for (Drink d : this.drinks) {
			Integer c = drinks.get(d.getName());
			if (null != c) {
				d.setCount(d.getCount() - c);
			}
		}

		for (Ingredient i : this.ingredients) {
			Integer c = ingredients.get(i.getName());
			if (null != c) {
				i.setCount(i.getCount() - c);
			}
		}
	}

	public void editDrink(int drinkId, int count, double price) {
		for (Drink d : drinks) {
			if ((d.getDrinkId() == drinkId)) {
				d.setCount(count);
				d.setPrice(price);
				break;
			}
		}
	}

	public void editIngredient(int ingredientId, int count, double price) {
		for (Ingredient i : ingredients) {
			if ((i.getIngredientId() == ingredientId)) {
				i.setCount(count);
				i.setPrice(price);
				break;
			}
		}
	}

	@Override
	public void Register(String name, String user, String password,
			String account_N) throws DataInterfaceException {
		// TODO Auto-generated method stub
		
	}

}
