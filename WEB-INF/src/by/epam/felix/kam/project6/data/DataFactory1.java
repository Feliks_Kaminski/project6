package by.epam.felix.kam.project6.data;

import by.epam.felix.kam.project6.Project6;
import by.epam.felix.kam.project6.initializer.Initializer;
import by.epam.felix.kam.project6.myexception.DataInterfaceException;
/**
 * Creates datasource for storing and getting data.
 * 
 * @author Felix
 *
 */
public class DataFactory1 extends DataFactoryAbstract {
	private static final DataFactory1 factory = new DataFactory1();
	private DataInterface dataSource;

	private DataFactory1() {

	}

	
	/**
	 * Selects the dataSource to work with. Cannot be changed while restarts servlet.
	 * 
	 * @param name String - dataSource type.
	 * @throws DataInterfaceException
	 */
	
	public void init(String name) throws DataInterfaceException {
		try{
			if (null == dataSource) {
				if (name.equals("HC")) {
					dataSource = new DataHardCode();
				}
				if (name.equals("DB")) {
					String path =  Project6.link().getServletContext().getRealPath("/")+"\\";
					String mainFilePath = path+Project6.link().getServletContext().getInitParameter("mainPropertyFile");
					String sqlFilesPath = path+Project6.link().getServletContext().getInitParameter("sqlFiles");
					Initializer initializer = new Initializer(mainFilePath);
					dataSource = new DataDB(initializer.getValue("DBurl"),
							initializer.getValue("DBuser"),
							initializer.getValue("DBpassword"),
							Integer.parseInt(initializer.getValue("DBconcount")),
							sqlFilesPath
					);
				}
			}
		} catch (Exception e){
			throw new DataInterfaceException(e);
		}
	}

	public static DataFactory1 getDataFactory1() {
		return factory;
	}

	public DataInterface getDataSource() {
		return dataSource;
	}
}
