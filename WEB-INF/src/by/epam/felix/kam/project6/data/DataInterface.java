package by.epam.felix.kam.project6.data;

import java.util.Set;

import by.epam.felix.kam.project6.myexception.DataInterfaceException;
import by.epam.felix.kam.project6.order.Drink;
import by.epam.felix.kam.project6.order.Ingredient;
import by.epam.felix.kam.project6.order.Order;
import by.epam.felix.kam.project6.userinfo.UserInfo;
/**
 * Implementing methods to work with different datasources.
 * 
 * @author Felix
 */
public interface DataInterface {
	/**
	 * Returns Set of available Drinks and it`s counts.
	 * 
	 * @return Set<Drink> - Drinks from datasource. Drink contains it`s name, count, price and id. 
	 * @throws DataInterfaceException
	 */
	public Set<Drink> getDrinks() throws DataInterfaceException;
	/**
	 * Returns Set of available Ingredients and it`s counts.
	 * 
	 * @return Set<Ingredient> - Ingredients from datasource. Ingredient contains it`s name, count, price and id. 
	 * @throws DataInterfaceException
	 */
	public Set<Ingredient> getIngredients() throws DataInterfaceException;

	/**
	 * Locates datasource information about user by it`s name and password. 
	 * 
	 * @param user - String
	 * @param password - String
	 * @return UserInfo - contains users nick ,name, id and access Level.
	 * @throws DataInterfaceException
	 */
	public UserInfo getUser(String user, String password)
			throws DataInterfaceException;
	
	
	/**
	 * Returns amount of money in user`s account.
	 * 
	 * @param userId int - needs id of the user to find his account.
	 * @return double - account`s content.
	 * @throws DataInterfaceException
	 */
	public double getAccountsRest(int userId) throws DataInterfaceException;
	
	/**
	 * Making Order: verifies count of contents in Order with rests, 
	 * reduces amount of Drinks and Ingredients in rests by it`s count in Order, 
	 * gets users money from account.
	 * 
	 * @param userId int - user id in datasource.
	 * @param order - Order. Holds at least 1 Drink. Any Drink can holds any count of any Ingredient.
	 * @throws DataInterfaceException
	 */
	
	public void acceptOrder(int userId, Order order)
			throws DataInterfaceException;

	/**
	 * User with special access level (Admin) can change count and price of the Drink.
	 * 
	 * @param drinkId int - Drink`s id in datasource.
	 * @param count int - number of Drinks portions in datasource.
	 * @param price double - price of the Drink.
	 * @throws DataInterfaceException
	 */
	
	public void editDrink(int drinkId, int count, double price)
			throws DataInterfaceException;
	
	
	/**
	 * User with special access level (Admin) can change count and price of the Ingredient.
	 * 
	 * @param drinkId int - Ingredient`s id in datasource.
	 * @param count int - number of Ingredients portions in datasource.
	 * @param price double - price of the Ingredient.
	 * @throws DataInterfaceException
	 */
	public void editIngredient(int ingredientId, int count, double price)
			throws DataInterfaceException;
	
	/**
	 * Register new user with his account.
	 * 
	 * @param name - long name of the user.
	 * @param user - nick to log in
	 * @param password - to log in
	 * @param account_N - account number
	 * @throws DataInterfaceException
	 */
	
	public void Register(String name,String user,String password,String account_N)
			throws DataInterfaceException;
	
}
