package by.epam.felix.kam.project6.initializer;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

import by.epam.felix.kam.project6.myexception.InitializerException;
/**
 * Reads properties by name from property file.
 * 
 * @author Felix
 */
public class Initializer {
	private Properties memory = new Properties();
	
	
	/**
	 * Constructor needs a property file name.
	 * 
	 * @param propertyFile String
	 */
	
	public Initializer(String propertyFile) {
		try {
			memory.load(new FileReader(propertyFile));
		} catch (IOException e) {
			new InitializerException(e);
		}
	}

	/**
	 * Returns the value of the property by it`s name.
	 * 
	 * @param name String
	 * @return String - property value.
	 */
	
	public String getValue(String name) {
		return memory.getProperty(name);
	}

}
