package by.epam.felix.kam.project6.connectionspool;

import java.sql.Connection;

import by.epam.felix.kam.project6.myexception.MyException;
/**
 * Rules to work with DB connections.
 * 
 * 
 * @author Felix
 *
 */
public interface Pool {
	/**
	 * Returns Connection , if at least 1 is not busy,
	 * or waiting for a free Connection.
	 * 
	 * @return Connection
	 * @throws MyException
	 * @throws InterruptedException
	 */
	public Connection getConection() throws MyException, InterruptedException;

	/**
	 * Returns unused Connection to Pool
	 * 
	 * @param con Connection
	 * @throws MyException
	 */
	
	public void returnConection(Connection con) throws MyException;

}
