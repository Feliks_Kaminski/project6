package by.epam.felix.kam.project6.connectionspool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.log4j.Logger;

import by.epam.felix.kam.project6.myexception.ConnectionCreateException;
import by.epam.felix.kam.project6.myexception.WrongConnectionException;

public class ConnectionsPool implements Pool {
	public static Logger logger = Logger.getLogger(ConnectionsPool.class
			.getName());
	private static final int timeOut = 300;
	private String url;
	private String user;
	private String password;
	private BlockingQueue<Connection> connectionsAll, connectionsFree;
	private final Lock editConnections = new ReentrantLock();

	public ConnectionsPool(String url, String user, String password, int count)
			throws ConnectionCreateException {
		this.url = url;
		this.user = user;
		this.password = password;
		connectionsAll = new ArrayBlockingQueue<Connection>(count);
		connectionsFree = new ArrayBlockingQueue<Connection>(count);
		Connection con = createConnection();
		connectionsAll.add(con);
		connectionsFree.add(con);
	}

	public Connection getConection() throws ConnectionCreateException,
			WrongConnectionException, InterruptedException {
		Connection con = connectionsFree.poll();
		if (null == con) {
			editConnections.lock();
			if (connectionsAll.remainingCapacity() > 0) {
				con = createConnection();
				connectionsAll.add(con);
				editConnections.unlock();
			} else {
				editConnections.unlock();
				con = connectionsFree.take();
			}

		}
		try {
			if (!con.isValid(timeOut)) {
				editConnections.lock();
				connectionsAll.remove(con);
				con = createConnection();
				connectionsAll.add(con);
				editConnections.unlock();
			}
		} catch (SQLException e) {
			logger.error("Wrong timeOut", e);
			throw new WrongConnectionException(e);
		}
		return con;
	}

	public void returnConection(Connection con)
			throws WrongConnectionException, ConnectionCreateException {
		try {
			if (connectionsAll.contains(con) && !connectionsFree.contains(con)) {
				if (con.isValid(timeOut)) {
					connectionsFree.add(con);
				} else {
					editConnections.lock();
					connectionsAll.remove(con);
					con = createConnection();
					connectionsAll.add(con);
					connectionsFree.add(con);
					editConnections.unlock();
				}
			} else {
				throw new WrongConnectionException();
			}
		} catch (SQLException e) {
			logger.error("Wrong timeOut");
			throw new WrongConnectionException(e);
		}
	}

	private Connection createConnection() throws ConnectionCreateException {

		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
			return DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			logger.error("Create con " + url + " " + user + " " + password);
			throw new ConnectionCreateException(e);
		}
	}

}