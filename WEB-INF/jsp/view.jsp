<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org./TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html: charset=utf-8">
        <title>Coffee-machine</title>
    </head>
    <body>
        <table>
            <tr>
                <td><h1>${_Hi} ${userInfo.name}</h1></td>
                <td>
                    <form action="Project6" method="post">
                        <input type="hidden" name="command" value="logoff"/>
                        <input type="submit" value="${_Logoff}"/>
                    </form>
                </td>
                <td>
                    <p>${message}</p>
                </td>
                <td>
                    <form action="Project6" method="post">
                        <input type="hidden" name="command" value="langchange"/>
                        <input type="hidden" name="lang" value="EN"/>
                        <input type="hidden" name="page" value="viewpage"/>
                        <input type="submit" value="EN"/>
                    </form>
                </td>
                <td>
                    <form action="Project6" method="post">
                        <input type="hidden" name="command" value="langchange"/>
                        <input type="hidden" name="lang" value="RU"/>
                        <input type="hidden" name="page" value="viewpage"/>
                        <input type="submit" value="RU"/>
                    </form>
                </td>
            </tr>
        </table>
        <p>${_Your_money} ${money}</p>
        <table>
            <tr>
                <td valign="top">
                    <table border=3>
                        <tr>
                            <td valign="top">    
                                <table border=3 width="300">
                                    <tr>
                                        <td align="center" colspan="3">${_Drinks}</td>
                                    </tr>
                                    <tr>
                                        <td>${_Name}</td>
                                        <td>${_Price}</td>
                                        <td>${_Portions}</td>
                                    </tr>
                                    <c:forEach var="drink" items="${drinks}" varStatus="status">
                                        <tr>
                                            <td>
                                                <form action="Project6" method="post">
                                                    <input type="hidden" name="command" value="addDrink"/>
                                                    <input type="hidden" name="drink" value="${drink.name}"/>
                                                    <input type="hidden" name="drinkId" value="${drink.drinkId}"/>
                                                    <input type="submit" value="${drink.name}"/>
                                                </form>
                                            </td>
                                            <td align="right"><c:out value="${drink.price}" /></td>
                                            <td align="right"><c:out value="${drink.count}" /></td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table border=3 width="300">
                                    <tr>
                                        <td align="center" colspan="3">${_Ingredients}</td>
                                    </tr>
                                    <tr>
                                        <td>${_Name}</td>
                                        <td>${_Price}</td>
                                        <td>${_Portions}</td>
                                    </tr>
                                    <c:forEach var="ingredient" items="${ingredients}" varStatus="status">
                                        <tr>
                                            <td>
                                                <form  action="Project6" method="post">
                                                    <input type="hidden" name="command" value="addIngredient"/>
                                                    <input type="hidden" name="ingredient" value="${ingredient.name}"/>
                                                    <input type="hidden" name="ingredientId" value="${ingredient.ingredientId}"/>
                                                    <input type="submit" value="${ingredient.name}"/>
                                                </form>
                                            </td>
                                            <td align="right"><c:out value="${ingredient.price}" /></td>
                                            <td align="right"><c:out value="${ingredient.count}" /></td>
                                        </tr>
                                    </c:forEach>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td valign="top">
                    <table border=3 width="300">
                        <tr>
                            <td colspan="2">${_Order}</td>
                            <td>
                                <form action="Project6" method="post">
                                    <input type="hidden" name="command" value="clearOrder"/>
                                    <input type="submit" value="${_Clear}"/>
                                </form>
                            </td>
                            <td>
                                <form action="Project6" method="post">
                                    <input type="hidden" name="command" value="buy"/>
                                    <input type="submit" value="${_Buy}"/>
                                </form>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">${_Total_cost}</td>
                            <td align="center" colspan="2">${totalCost}</td>
                        </tr>
                        <c:forEach var="drink" items="${order.drinks}" varStatus="statusD">
                            <tr>
                                <td colspan="2">
                                    <c:out value="${drink.name}" />
                                </td>
                                <td>
                                    <c:out value="${drink.price}" />
                                </td>
                                <td>
                                    <form action="Project6" method="post">
                                        <input type="hidden" name="command" value="removeDrink"/>
                                        <input type="hidden" name="drink" value="${drink.name}"/>
                                        <input type="hidden" name="drinkId" value="${drink.drinkId}"/>
                                        <input type="hidden" name="drinkN" value="${statusD.count}"/>
                                        <input type="submit" value="${_Remove}"/>
                                    </form>
                                </td>
                            </tr>
                            <c:forEach var="ingredient" items="${drink.ingredients}" varStatus="statusI">
                            <tr>
                                <td></td>
                                <td>
                                    <c:out value="${ingredient.name}" />
                                </td>
                                <td>
                                    <c:out value="${ingredient.price}" />
                                </td>
                                <td>
                                    <form action="Project6" method="post">
                                        <input type="hidden" name="command" value="removeIngredient"/>
                                        <input type="hidden" name="ingredient" value="${ingredient.name}"/>
                                        <input type="hidden" name="ingredientId" value="${ingredient.ingredientId}"/>
                                        <input type="hidden" name="drinkN" value="${statusD.count}"/>
                                        <input type="hidden" name="ingredientN" value="${statusI.count}"/>
                                        <input type="submit" value="${_Remove}"/>
                                    </form>
                                </td>
                            </tr>
                            </c:forEach>
                        </c:forEach>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>