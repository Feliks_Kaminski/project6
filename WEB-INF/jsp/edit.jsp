<%@page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org./TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html: charset=utf-8">
        <title>Coffee-machine</title>
    </head>
    <body>
        <table>
            <tr>
                <td><h1>Hi ${userInfo.name}</h1></td>
                <td>
                    <form action="Project6" method="post">
                        <input type="hidden" name="command" value="logoff"/>
                        <input type="submit" value="Logoff"/>
                    </form>
                </td>
                <td>
                    <p>${message}</p>
                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td valign="top">
                    <table border=3>
                        <tr>
                            <td valign="top">    
                                <table border=3 width="650">
                                    <tr>
                                        <td align="center" colspan="6">Drinks</td>
                                    </tr>
                                    <tr>
                                        <td width="150">Name</td>
                                        <td>Price</td>
                                        <td>New price</td>
                                        <td>Count</td>
                                        <td>New count</td>
                                        <td></td>
                                    </tr>
                                    <c:forEach var="drink" items="${drinks}" varStatus="status">
                                        <tr>
                                            <form action="Project6" method="post">
                                                <input type="hidden" name="command" value="editDrink"/>
                                                <input type="hidden" name="drink" value="${drink.name}"/>
                                                <input type="hidden" name="drinkId" value="${drink.drinkId}"/>
                                                    <td>
                                                        <c:out value="${drink.name}" />
                                                    </td>
                                                    <td align="right">
                                                        <c:out value="${drink.price}" />
                                                    </td>
                                                    <td>
                                                        <input type="number" name="newprice" value="${drink.price}">
                                                    </td>
                                                    <td align="right">
                                                        <c:out value="${drink.count}" />
                                                    </td>
                                                    <td>
                                                        <input type="number" name="newcount" value="${drink.count}">
                                                    </td>
                                                    <td>
                                                        <input type="submit" value="Set"/>
                                                    </td>
                                            </form>                                            
                                        </tr>
                                    </c:forEach>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top">
                                <table border=3 width="650">
                                    <tr>
                                        <td align="center" colspan="6">Ingredients</td>
                                    </tr>
                                    <tr>
                                        <td width="150">Name</td>
                                        <td>Price</td>
                                        <td>New price</td>
                                        <td>Count</td>
                                        <td>New count</td>
                                        <td></td>
                                    </tr>
                                    <c:forEach var="ingredient" items="${ingredients}" varStatus="status">
                                        <tr>
                                            <form action="Project6" method="post">
                                                <input type="hidden" name="command" value="editIngredient"/>
                                                <input type="hidden" name="ingredient" value="${ingredient.name}"/>
                                                <input type="hidden" name="ingredientId" value="${ingredient.ingredientId}"/>
                                                    <td>
                                                        <c:out value="${ingredient.name}" />
                                                    </td>
                                                    <td align="right">
                                                        <c:out value="${ingredient.price}" />
                                                    </td>
                                                    <td>
                                                        <input type="number" name="newprice" value="${ingredient.price}">
                                                    </td>
                                                    <td align="right">
                                                        <c:out value="${ingredient.count}" />
                                                    </td>
                                                    <td>
                                                        <input type="number" name="newcount" value="${ingredient.count}">
                                                    </td>
                                                    <td>
                                                        <input type="submit" value="Set"/>
                                                    </td>
                                            </form>                                            
                                        </tr>
                                    </c:forEach>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </body>
</html>